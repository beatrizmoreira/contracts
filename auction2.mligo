// Map of bids for each client
// @key - client address
// @value - value of bid
type store = (int, int) map

//state of auction and storage of bids
type storage = {
  auctionEnded : int;
  highestBid   : int;
  highestBidder: int;
  store        : store
}

type act = {
  address : int
}

//address of user, and amount of bid
type bidding = {
  address : int;
  amount     : int
}

type return = operation list * store

//let owner : int = 0

type parameter =
//| Init of act
| Finish of act
| Bid of bidding
| Withdraw of act
| Win of act

//let init (a, storage : act * storage) : return =
//  let no_op : operation list = [] in
//  if a.address <> 0 then
 //   (failwith ("Not the owner of the contract.") : return)
//  else if storage.auctionEnded = 0 then
//    (failwith("The auction is already running") : return)
  //else
   // let ae : int = 0 in (no_op, {storage with auctionEnded = ae})

let finish (a, storage : act * storage) : return =
  let no_op : operation list = [] in
  let cond : bool = (a.address = 0 && storage.auctionEnded = 0) in
  if cond then
    let ae : int = 1 in (no_op, {storage with auctionEnded = ae})
  else
    (failwith ("Cannot terminate auction") : return)

let bid (bidding, storage : bidding * storage) : return =
  let no_op : operation list = [] in
  let b: int option = Map.find_opt bidding.address storage.store in
  if bidding.address = 0 then
    (failwith ("Owner cannot bid") : return)
  else if storage.auctionEnded = 1 then
    (failwith ("Auction is over") : return)
  else if bidding.address = storage.highestBidder then
    (failwith ("Client is already the highest bidder") : return)
  else if b > 0 then
    (failwith ("Already bid, need to withdraw first") : return)
  else if bidding.amount <= 0 then
    (failwith ("Value bid needs to be greater than 0") : return)
  else
    let s: storage = Map.update bidding.address (Some bidding.amount) storage.store in
    (([] : operation list), s)

let withdraw (a, storage : act * storage) : return =
  let no_op : operation list = [] in
  let b : int option = Map.find_opt a.address storage.store in
  if a.address = 0 then
    (failwith ("Owner cannot withdraw") : return)
  else if a.address = storage.highestBidder then
    (failwith ("Client is the highest bidder, cannot withdraw") : return)
  else if b > 0 then
    (failwith ("Client has no pending returns") : return)
  else
    let s : store = Map.update a.address (Some 0) storage.store in
    (no_op, {storage with storage.store = s})

let win (a, storage : act * storage) : return =
  let no_op : operation list = [] in
  let b : int option = Map.find_opt a.address storage.store in
  if a.address = 0 then
    (failwith ("Owner cannot win") : return)
  else if storage.auctionEnded = 0 then
    (failwith ("Auction is still running") : return)
  else if a.address <> storage.highestBidder then
    (failwith ("Isnt the winner of the auction") : return)
  else if b = 0 then
    (failwith ("Prize was already collected") : return)
  else
    let s : store = Map.update a.address (Some 0) storage.store in
    (no_op, {storage with storage.store = s})


let main (action, storage : parameter * storage) : return =
  match action with
  //| Init a  -> init (a, storage)
  | Finish a -> finish (a, storage)
  | Bid b -> bid (bidding, storage)
  | Withdraw a -> withdraw (a, storage)
  | Win a -> win (a, storage)
